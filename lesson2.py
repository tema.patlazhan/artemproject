# booleans

# my_bool = True
#
# my_bool += 3
# print(my_bool)
#
# print(bool(-1.8))
# print(bool(0.0))

# branching
# condition = True
# if condition:  # bool(condition)
#     print("It's True")
#     print("It's True")
# else:
#     print("It's False")
#     print("It's False")

# condition = -1234
# if condition:  # bool(condition)
#     print("It's True")
#     print("It's True")
# else:
#     print("It's False")
#     print("It's False")


# condition = -0
# if condition:  # bool(condition)
#     print("It's True")
#
# print("Next code")


# condition1 = 0
# condition2 = 0
# condition3 = 0
#
# if condition1:  # bool(condition)
#     print("1 It's True")
# elif condition2:  # bool(condition)
#     print("2 It's True")
# elif condition3:  # bool(condition)
#     print("3 It's True")
# else:
#     print("False")
#
# print("Next code")

# a = 10
# b = 20
#
# if a > b:
#     print("a > b")
# elif a < b:
#     print("a < b")
# else:
#     print("a = b")
#
# print("Next code")

# a = -10
# b = -50
#
# if a > b:
#     if a > 0:
#         print("a > 0")
#     print("a > b")
# elif a < b:
#     print("a < b")
# else:
#     print("a = b")
#

# a = -10
# b = -50
#
# if a > b:
#     c = 10
# else:
#     c = 20
#
# c = 10 if a > b else 20  # ternary operator


# looping (while)

# a = 5
# b = 0
#
# while a > b:  # bool(cond) -> True
#     print('function is going', a, b)
#     b += 1
#
# print('function ends')

# a = 5
# b = 0
#
# while True:
#     print('function is going', a, b)
#     b += 1
#     if a < b:
#         break

# a = 5
# b = 0
#
# while True:
#     b += 1
#     if a < b:
#         break  # skip loop
#     if b % 2 == 0:
#         continue  # skip just current iteration
#
#     print('function is going', a, b)


# a = 5
# b = 0
#
# while True:
#     b += 1
#     if a < b:
#         break
#
#     print('outer loop ---> ', a, b)
#     c = 1
#     while True:
#         print('inner loop', c)
#         c += 1
#         if c > 3:
#             break


# a = 5
# b = 0
#
# while True:
#     b += 1
#     if a < b:
#         break
#
#     print('outer loop ---> ', a, b)
#     c = 1
#     while True:
#         print('inner loop', c)
#         c += 1
#         if c > 3:
#             break

# a = 5
# b = 0
#
# while a > b:
#     if b == 4:
#         print('inside loop', a, b)
#     b += 1
# else:
#     print('loop is over')

# a = 5
# b = 0
#
# while a > b:
#     b += 1
#     if b != 4:
#         continue
#     print('inside loop', a, b)
# else:
#     print('loop is over')


# res = input('Enter something: ')
# print('Your enter is: ', res)

# strings

# my_str = 'a'
# my_str = 'ab'
# my_str = 'ablaksdfvbm;ngo'
# my_str = ''

# # my_str = "a"
# my_str = 'I\'m Groot'
# print(my_str)
# print(type(my_str))

# my_str = '1234'
# print(my_str)
# print(type(my_str))
#
# my_str = str(True)  # -> 'True'
# print(my_str)
# print(type(my_str))


# my_str = str(3.14)  # -> '3.14'
# print(my_str)
# print(type(my_str))

# my_str = '123' + 'abc'
#
# print(my_str)

# my_str1 = '123'
# my_str2 = 'abc'
# res = my_str2 + my_str1 + ")(*&"
# print(res)

# my_str2 = 'abc'
# res = my_str2 * 10
# print(res)

# my_str2 = '123.0'
# res = int(my_str2)
# print(res)
# print(type(res))


# my_str2 = '123.0'
# res = float(my_str2)
# print(res)
# print(type(res))

#
# my_str2 = ''
# res = bool(my_str2)
# print(res)
# print(type(res))


# name = 'Artem'
# age = 40
#
# # Hello, my name is Artem, I'm 40 years old
#
# res = 'Hello, my name is ' + name + ', I\'m ' + str(age) + ' years old'
# print(res)
#
# res = 'Hello, my name is %s, I\'m %s years old' % (name, age)
# print(res)
#
# tpl = 'Hello, my name is %s, I\'m %s years old'
# res = tpl % (name, age)
# print(res)
#
# tpl = 'Hello, my name is {}, I\'m {} years old'
# res = tpl.format(name, age)
# print(res)
#
# tpl = 'Hello, my name is {1}, I\'m {0} years old'
# res = tpl.format(name, age)
# print(res)
#
# tpl = 'Hello, my name is {name_value}, I\'m {age_value} years old'
# res = tpl.format(name_value=name, age_value=age)
# print(res)
#
# res = f'Hello, my name is {name}, I\'m {age} years old'
# print(res)


# my_str = "Hello, my name is Artem, I'm 40 years old"
#
# # isdigit()
# print(my_str.isdigit())
#
# my_str = '10'
# if my_str.isdigit():
#     res = int(my_str)
#
# print("Hello" == "Hello")


# # isalpha()
# my_str = "Hellomy"
# print(my_str.isalpha())

# isalnum()
# my_str = "Hellomy1234"
# print(my_str.isalnum())

# my_str = "Hellomy1234"
# print('Hell' in my_str)

# my_str = "hellomy"
# print(my_str.islower())
#
# my_str = "HELLO"
# print(my_str.isupper())

# my_str = "hello"
# print(my_str.upper())
#
# my_str = "Hello"
# print(my_str.lower())
#
# my_str = "hello mY nAme is"
# print(my_str.capitalize())
#
# my_str = "hello mY nAme is"
# print(my_str.title())

# my_str = "hello mY nAme is"
# res = my_str.replace('m', '+')
# print(my_str)
# print(res)

# my_str = " hello mY nAme is"
# res = my_str.startswith("hell")
# print(res)
#
# my_str = " hello mY nAme is"
# res = my_str.endswith(" is")
# print(res)

# my_str = " hello mY nAme is"
# res = my_str.count('mes')
# print(res)
#
# my_str = "010hello mY nAme is020"
# res = my_str.strip("012")
# print(res)
#
# my_str = "010hello mY nAme is020"
# res = my_str.lstrip("012")
# print(res)

# my_str = "010hello mY nAme is020"
# res = my_str.rstrip("012")
# print(res)

# my_str = "hello my name is Artem"
# res = my_str.split()  #
# print(res)

# my_str = 'hello my name is Artem'
# res = my_str.split('e')
# print(res)


"""
ДЗ 1
Напишіть программу "Касир в кінотеватрі", яка буде виконувати наступне:
Попросіть користувача ввести свсвій вік (можно використати input()).
Програма не повинна піти далі, якщо користувач після запрошення ввести вік ввів не число!
- якщо користувачу менше 6 - вивести повідомлення"Де твої батьки?"
- якщо користувачу менше 16 - вивести повідомлення "Це фільм для дорослих!"
- якщо користувачу більше 65 - вивести повідомлення "Покажіть пенсійне посвідчення!"
- якщо вік користувача містить цифру 7 - вивести повідомлення "Вам пощастить!"
- у будь-якому іншому випадку - вивести повідомлення "А білетів вже немає!"

P.S. На екран має бути виведено лише одне повідомлення, якщо вік користувача містить цифру 7 - тільки
відповідне повідомлення! Також подумайте над варіантами, коли введені невірні або неадекватні (неможливі) дані.

"""