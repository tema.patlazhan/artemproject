import random


class GameFigure:

    options = {
        "R": "S",
        "P": "R",
        "S": "P",
    }
    name = None

    def __init__(self, name):
        if name not in self.options:
            raise Exception
        self.name = name

    def _check_type(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError

    def __eq__(self, other):
        self._check_type(other)
        return self.name == other.name

    def __ne__(self, other):
        self._check_type(other)
        return self.name != other.name

    def __gt__(self, other):
        self._check_type(other)
        return self.options[self.name] == other.name

    def __lt__(self, other):
        self._check_type(other)
        return self.options[self.name] != other.name


class BasePlayer:

    name = None

    def get_figure(self):
        return self._get_figure()

    def _get_figure(self):
        raise NotImplementedError


class User(BasePlayer):

    def __init__(self):
        self.name = input('Enter your name: ')

    def _get_figure(self):
        hint = list(GameFigure.options.keys())
        while True:
            player_choice = input(f"Rock, Paper, Scissors, SHOOT choice your item from the list {hint}:  ").upper()
            if player_choice in GameFigure.options.keys():
                break
        return GameFigure(player_choice)


class AI(BasePlayer):

    def __init__(self):
        self.name = 'Terminator'

    def _get_figure(self):
        dealer_figure_name = random.choice(list(GameFigure.options.keys()))
        fig = GameFigure(dealer_figure_name)

        return fig


def game_launcher():
    player = User()
    dealer = AI()
    # ->
    player_choice = player.get_figure()
    dealer_choice = dealer.get_figure()

    if player_choice > dealer_choice:
        print(f'Winner is {player.name}')
    elif player_choice < dealer_choice:
        print(f'Winner is {dealer.name}')
    else:
        print(f'Tie')
    # player.continue()


game_launcher()
