# # functions
# PI = 3.14
# a = 10
#
# variable = 10
#
#
# def foo(a):
#
#     # variable = 20
#     print(f'---> {a}')
#     a += 1
#
#     return a, 1, 2
#
#
# a, b, c = foo(a)
#
#
# # namespace
#
# # built-in
# #   global
# #      local
#
#
# print(dir())


# global_variable = []
#
#
# def foo():
#     global_variable.append(1)
#     print(f'---> {global_variable}')
#
#
# foo()


# global_variable = 1
#
#
# def foo():
#     global global_variable
#
#     global_variable += 1
#     print(f'---> {global_variable}')
#
#
# print(global_variable)
#
# foo()
#
# print(global_variable)


# global_variable = 1
#
#
# def foo():
#     global_variable = 100
#
#     global_variable += 1
#     print(f'---> {global_variable}')
#
#
# print(global_variable)
#
# foo()
#
# print(global_variable)


# def foo(a, b, c):
#     """
#     Docstring
#     :param a:
#     :type a: int
#     :param b:
#     :type b: int
#     :param c:
#     :type c: int
#     :return:
#     :rtype: tuple
#     """
#     global_variable = 100
#
#     global_variable += 1
#     print(f'---> {global_variable}')
#
#     return 123, 2
#
# print(help(foo))
# print(foo.__doc__)
#
# foo('123', 1, 2)


# def foo(a: int, b: float, c: list) -> tuple:
#     """"""
#     global_variable = 100
#
#     global_variable += 1
#     print(f'---> {global_variable}')
#
#     return 123, 2
#
#
# res = foo('123', 1, 2)
#
# print(help(foo))





