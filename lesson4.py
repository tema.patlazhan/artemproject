# comprehensions

# lst1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 10, 33, 44, -5]
#
# lst2 = []
# for list_item in lst1:
#     if list_item % 2 != 0:
#         lst2.append(list_item)
#
# print(lst1)
# print(lst2)

# lst1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 10, 33, 44, -5]
#
# lst2 = [list_item for list_item in lst1 if list_item % 2 != 0]
#
# print(lst1)
# print(lst2)

# lst1 = [1, 2, 3, 4, 5, -6, 7, 8, 9, 0, -10, 33, 44, -5]

# lst2 = [str_item for str_item in '12hgjh12j3jhg13324jhgjhg13jhgjg242g']

# lst2 = [i ** 2 if i > 0 else 0 for i in lst1 if i % 2 == 0]

# print(lst1)
# print(lst2)


# lst1 = [1, 2, 3, 4, 5, -6, 7, 8, 9, 0, -10, 33, 44, -5]
#
# # lst1 = lst1 + [1]
# lst1.append(1)
#
# print(lst1)

# lst1 = [1, 2, 3]
# lst2 = lst1
#
# lst2.append(4)
#
# print('lst1', lst1)
# print('lst2', lst2)

# a = 10
# b = a
# print(id(a), a)
# print(id(b), b)
#
# b += 1
#
# print(id(a), a)
# print(id(b), b)
#
# #
# x = 'qwert'
# print(dir())


# lst1 = [1, 2, 3]
# lst2 = lst1
# print('lst1', id(lst1))
# print('lst2', id(lst2))
#
# lst2.append(4)
#
# print('lst1', id(lst1), lst1)
# print('lst2', id(lst2), lst2)

# lst1 = [1, 2, 3]
#
# lst2 = lst1.copy()
#
# print('lst1', id(lst1))
# print('lst2', id(lst2))
#
# lst2.append(4)
#
# print('lst1', id(lst1), lst1)
# print('lst2', id(lst2), lst2)


# lst1 = [[], 1, 2, 3]
#
# lst2 = lst1.copy()
#
# print('lst1', id(lst1))
# print('lst2', id(lst2))
#
# lst2.append(4)
#
# print('lst1', id(lst1), lst1)
# print('lst2', id(lst2), lst2)
#
# lst2[0].append('a')
#
# print('lst1', id(lst1), lst1)
# print('lst2', id(lst2), lst2)
#
#
# print('lst1[0]', id(lst1[0]), lst1)
# print('lst2[0]', id(lst2[0]), lst2)

# import copy
#
# lst1 = [[], 1, 2, 3]
#
# lst2 = copy.deepcopy(lst1)
#
# print('lst1', id(lst1))
# print('lst2', id(lst2))
#
# lst2.append(4)
#
# print('lst1', id(lst1), lst1)
# print('lst2', id(lst2), lst2)
#
# lst2[0].append('a')
#
# print('lst1', id(lst1), lst1)
# print('lst2', id(lst2), lst2)
#
#
# print('lst1[0]', id(lst1[0]), lst1)
# print('lst2[0]', id(lst2[0]), lst2)


# tuple

# my_tuple = (1, 2, 3, 4, 5, 6, 7, 8)
# print(my_tuple)
# print(my_tuple[0])
# print(my_tuple[0:3])
#
# for i in my_tuple:
#     print(i)
#
# my_tuple = my_tuple + ('a', 'b')
# print(my_tuple)
#
#
# # set
#
# my_set = {1, 2, 3, 4, 6}
# print(my_set)
#
# my_set = {1, 1, 1, 2, 2, 2, 3, 3, 4, 4}
# print(my_set)
#
# my_set.add(0)
# print(my_set)
# my_set.add(10)
# print(my_set)
#
# print(0 in my_set)
#
# for i in my_set:
#     print(i)

# lst = [1, 2, 3]
#
# print(tuple(lst))
# print(list((1, 2, 3, 4)))
#
# my_set = set((1, 2, 3, 4))
# print(my_set)
#
#
# print(set('asdfasdfgasdfgh'))
# print(list('asdfasdfgasdfgh'))
# print(tuple('asdfasdfgasdfgh'))
#
# lst = [1, 2, 3, 1, 2, 3, 1, 2, 4, 3]
# lst = list(set(lst))
# print(lst)
#
# lst = frozenset([1, 2, 3, 1, 2, 3, 1, 2, 4, 3])

# lst = [i ** 2 for i in range(1, 10)]
# print(lst)
#
# my_set = {i ** 2 for i in range(1, 10)}
# print(my_set)
#
# my_tuple = (i ** 2 for i in range(1, 10))
# print(my_tuple)


# lst = [i ** 2 for i in range(10 ** 100)]
# my_generator = (i ** 2 for i in range(100 ** 100) if i % 2 == 0)
# print(my_generator)


# lst = [1, 2, 3, 1, 2, 3, 1, 2, 4, 3]
#
# for i in lst:
#     # print(i)
#     lst.remove(i)
#
#
# print(lst)


# lst = [1, 2, 3, 1, 2, 3, 1, 2, 4, 3]
#
# if len(lst) > 5:
#     print(lst[5])
# else:
#     print(lst[0])
#

# try except construct

# try:
#     res = int(input('Enter some number: '))
# except:
#     print('Must be a number')


# try:
#     res = 1 / int(input('Enter some number: '))
# except ValueError:
#     print('Must be a number')
# except ZeroDivisionError:
#     print('Not a zero!')
#
# print('Here')

# try:
#     res = 1 / int(input('Enter some number: '))
# except (ValueError, ZeroDivisionError):
#     print('Must be number (not a 0)')
#
# print('Here')
#
# try:
#     res = 1 / int(input('Enter some number: '))
# except Exception:
#     print('Must be number (not a 0)')
#
# print('Here')

# try:
#     res = 1 / int(input('Enter some number: '))
# except ValueError:
#     print('Must be number (not a 0)')
# except:
#     print('Error')
#
# print('Here')


# try:
#     res = 1 / int(input('Enter some number: '))
# except Exception as err:
#     print(f'error {err}')


# try:
#     res = 1 / int(input('Enter some number: '))
# except Exception as err:
#     print(f'error {err}')
# else:
#     print('No error')
#
# print('Here')


# try:
#     res = 1 / int(input('Enter some number: '))
# except Exception as err:
#     print(f'Error {err}')
# else:
#     print('No error')
# finally:
#     print('Finally')
#
# print('Here')


# try:
#     res = float(input('Enter some number: '))
# except Exception as err:
#     print(f'Error {err}')
# else:
#     print('No error')
# finally:
#     print('Finally')
#
# print('Here')


"""
1. Є list довільних чисел, наприклад [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44].
Напишіть код, який видалить з нього всі числа, які менше 21 і більше 74.

2. Є стрінг з певним текстом. Напишіть код, який визнаачить кількість слів в цьому тексті,
які починаються з голосної ( aoiue ) літери.


"""