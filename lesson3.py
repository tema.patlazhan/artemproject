# string indexing

# my_str = 'abcdef'  # 0 1 2 3 4 5
# # print(my_str[6])
# res = my_str[1]
# print(type(res))
# print(len(res))
# print(len(my_str))
#
# print(my_str.find('ab'))

'abcdef'
'012345'
' a b c d e f'
'-6-5-4-3-2-1'

# my_str = 'abcdef'  # 0 1 2 3 4 5
# res = my_str[-5]
# print(res)
#
#
# # string slices
# my_str = 'abcdef'  # 0 1 2 3 4 5
# res = my_str[2:4]
# print(res)
#
# 'ab|cd|ef'
# '01|23|45'

# my_str = 'abcdefghik'
# print('my_str[2:6]', my_str[2:6])
# print('my_str[:4]', my_str[:4])
# print('my_str[3:]', my_str[3:])
#
# print('my_str[:-3]', my_str[:-3])
# print('my_str[-4:]', my_str[-4:])
# print('my_str[-7:-2]', my_str[-7:-2])
# print('my_str[-2:-7]', my_str[-2:-7])
# print('my_str[2:7:2]', my_str[2:7:2])
# print('my_str[::-1]', my_str[::-1])
# begin = 2
# end = 7
# step = 2
# print('my_str[begin:end:step]', my_str[begin:end:step])
# print('my_str[len(my_str) // 2:]', my_str[len(my_str) // 2:])
# print('my_str[int(len(my_str) / 2):]', my_str[int(len(my_str) / 2):])


# srt_age = '1 22'
# srt_age = '222'
# srt_age = '22 1'
# srt_age = '1221'

# srt_age = '122'
# idx = 0
# previous = ''
# while idx < len(srt_age):
#     print(srt_age[idx])
#     current = srt_age[idx]
#     if current == previous:
#         print('match!')
#         break
#     previous = current
#     idx += 1
#
# print('end of loop')


# srt_age = '13233'
# idx = 0
#
# while idx < len(srt_age) - 1:
#     print(srt_age[idx])
#     if srt_age[idx] == srt_age[idx + 1]:
#         print('match!')
#         break
#     idx += 1
#
# print('end of loop')


# looping (for)
#
# my_str = 'qwwertyui'
#
# previous = ''
# for char in my_str:
#     if previous == char:
#         print('match')
#         break
#     previous = char
#     print('->', char)


# list

# my_str = 'hello world'
# res = my_str.split()
#
# print(res)
# print(type(res))

# my_list = []
# # my_list = list()
#
# print(my_list)
# print(type(my_list))
#
# my_list = [1, 2.3, True, 'FOUR', None, 1, 2.3, [1, 2, 3]]

# print(my_list[2:4])
# print(my_list[-1])

# my_list = [1, 2.3, True, 'FOUR', None, 1, 2.3, None, [1, 2, 3], None]
#
# for list_item in my_list:
#     print(list_item, type(list_item))
#
#
# my_list[-1] = '123456 sdfgh'
# print(my_list)
#
# my_list.append(123456)
# print(my_list)
#
# my_list.pop(1)
# print(my_list)
#
# my_list.remove(None)
# print(my_list)
#
# lst1 = [1, 2, 3]
# lst2 = ['a', 'b', 'c']
#
# my_list = lst1 + lst2
#
# print(my_list)
#
# my_list *= 2
# print(my_list)
#
# lst = []
# lst.append([])
# lst.append([])
# lst[0].append(0)
# lst[1].append(1)
# print(lst)

# lst = [[0, 0], [1, 1]]
# lst = [[0, 0, 0], [1, 1, 1], [2, 2, 2]]

# print(lst)

"""
HW 
Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']. 
Напишіть код, який? свормує новий list (наприклад lst2), який містить лише змінні типу стрінг, які присутні в lst1. 
Зауважте, що lst1 не є статичним і може формуватися динамічно.

"""
