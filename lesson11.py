# decorator


# def decorator(func):
#
#     def wrapper(*args, **kwargs):
#         print('Before function')
#         res = func(*args, **kwargs)
#         print('After function')
#         return res
#
#     return wrapper
#
#
# def function_to_decorate():
#     print('Hello, I\'m function_to_decorate')
#
#
# def function_to_decorate2():
#     print('Hello, I\'m function_to_decorate2')

# function_to_decorate()


# decorated_function = decorator(function_to_decorate)
#
# decorated_function()
#
# decorated_function2 = decorator(function_to_decorate2)
#
# decorated_function2()


# function_to_decorate = decorator(function_to_decorate)
#
# function_to_decorate()


# def decorator(func):
#
#     def wrapper(*args, **kwargs):
#         print('Before function')
#         res = func(*args, **kwargs)
#         print('After function')
#         return res
#
#     return wrapper
#
#
# @decorator  # function_to_decorate = decorator(function_to_decorate)
# def function_to_decorate():
#     print('Hello, I\'m function_to_decorate')
#
#
# @decorator
# def function_to_decorate2():
#     print('Hello, I\'m function_to_decorate2')
#
#
# function_to_decorate()


# def only_int_float(func):
#
#     def wrapper(*args, **kwargs):
#         for arg in args:
#             if not isinstance(arg, (int, float)):
#                 raise TypeError
#         for val in kwargs.values():
#             if not isinstance(val, (int, float)):
#                 raise TypeError
#
#         return func(*args, **kwargs)
#
#     return wrapper
#
#
# @only_int_float
# def my_calc(var1: int | float, var2: int | float) -> int | float:
#
#     return var1 * var2
#
#
# res = my_calc(1, '2')
# print(res)


# def decorator1(func):
#     def wrapper(*args, **kwargs):
#         print('decorator1 - Before function')
#         res = func(*args, **kwargs)
#         print('decorator1 - After function')
#         return res
#
#     return wrapper
#
#
# def decorator2(func):
#     def wrapper(*args, **kwargs):
#         print('decorator2 - Before function')
#         res = func(*args, **kwargs)
#         print('decorator2 - After function')
#         return res
#
#     return wrapper
#
#
# @decorator1
# @decorator2    # function_to_decorate = decorator1(decorator2(function_to_decorate))
# def function_to_decorate():
#     print('Hello, I\'m function_to_decorate')
#
#
# function_to_decorate()

#
# def check_types(*types):
#
#     def decorator(func):
#
#         def wrapper(*args, **kwargs):
#             for arg in args:
#                 if not isinstance(arg, types):
#                     raise TypeError
#             for val in kwargs.values():
#                 if not isinstance(val, types):
#                     raise TypeError
#
#             return func(*args, **kwargs)
#
#         return wrapper
#
#     return decorator
#
#
# # decorator = check_types(int)
# # my_calc = decorator(my_calc)
# @check_types(int, float)
# def my_calc(var1: int | float, var2: int | float) -> int | float:
#
#     return var1 * var2
#
#
# my_calc(1, 2)


# useful functions

# res = int(2.9)
# print(res)
#
# res = float(2)
# print(res)
#
#
# res = set('123456')
# print(res)


# res = dict(a=10, b=20)
# print(res)
#
#
# res = dict([(1, 2), (2, 3), (3, 4)])
# print(res)


# data = [1, 23, 3, 44, 5, 6, 7, 8]
#
# print(max(122, 23, 3, 44, 5, 6, 7, 8))
#
#
#
# def foo(v):
#     return v % 3
#
#
# data = [1, 23, 3, 44, 5, 6, 7, 8]
#
# print(max(data, key=foo))
# print(max(data, key=lambda v: v % 3))
# print(min(data, key=lambda v: v % 3))



"""
Homework

1. Напишіть декоратор, який перетаорює результат роботи буль-якої функції на стрінги

2. Наишіть декоратор, який обмежує кількисть аргумантів функції до 3-х

3. Наишіть декоратор, який перехоплює помилки при виконанні функції. 
Якщо функція підняла помилку - декоратор повинен сказати про помилку і повернути None


"""

var1 = 100
var2 = 200


def example_function():
    print('example_function')


if __name__ == '__main__':
    example_function()
