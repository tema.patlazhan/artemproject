# dict
# lst = [1, '2', 3, None]
# print(lst[3])

# my_dict = {
#     0: '1234567',
#     1.2: 12345.2345,
#     True: False,
#     'some text': 'some text',
#     (1, 2, 3): [1, 2, 3],
#     None: None,
#     100: {},
# }
#
# print(my_dict[100])
#
# my_dict = {
#     'login': '1234567',
#     'password': 'password',
#     'last enter': '12.12.2024',
# }
#
#
# print(my_dict['login'])

# my_dict = {
#     0: '1234567',
#     1.2: 12345.2345,
#     True: False,
#     'some text': 'some text',
#     (1, 2, 3): [1, 2, 3],
#     None: None,
#     100: {},
# }
#
#
# print(my_dict[True])
#
# my_dict[True] = '12345678'
# print(my_dict[True])
#
# my_dict['blabla'] = '123456789'
# print(my_dict['blabla'])
# print(my_dict)
#
# # print(my_dict['qwerty'])

# my_dict = {
#     0: '1234567',
#     1.2: 12345.2345,
#     True: False,
#     'some text': 'some text',
#     (1, 2, 3): [1, 2, 3],
#     None: None,
#     100: {},
# }


# for i in my_dict:  # i is key
#     print(i)
#
# for key in my_dict.keys():  # key is dict key
#     print(key)
#
# for key in my_dict.keys():  # key is dict key
#     print(f'key is {key}, value is {my_dict[key]}')


# for value in my_dict.values():  # value is dict value
#     print(value)

#
# for item in my_dict.items():  # item is (key, value)
#     print(f'key is {item[0]}, value is {item[1]}')
#
# for key, value in my_dict.items():
#     print(f'key is {key}, value is {value}')


# my_dict = {
#     0: '1234567',
#     1.2: 12345.2345,
#     True: False,
#     'some text': 'some text',
#     (1, 2, 3): [1, 2, 3],
#     None: None,
#     100: {},
# }
#
# print(10 in my_dict)
# print(10 in my_dict.keys())
# print(10 in my_dict.values())
#
# if 10 in my_dict:
#     print(my_dict[10])


# my_dict = {
#     0: '1234567',
#     1.2: 12345.2345,
#     True: False,
#     'some text': 'some text',
#     (1, 2, 3): [1, 2, 3],
#     None: None,
#     100: {},
#     10: 'here I am!'
# }
#
# # print(my_dict.get(10))
# print(my_dict.get(10, 'default value'))
#
# my_dict = {
#     0: '1234567',
#     1.2: 12345.2345,
#     True: False,
#     'some text': 'some text',
#     (1, 2, 3): [1, 2, 3],
#     None: None,
#     100: {},
#     10: 'here I am!',
# }
#
# print(my_dict)
#
# my_dict[123] = 321
#
# print(my_dict)
#
# my_dict.pop(123)
# print(my_dict)
#
# my_dict.update({100: 1, 'b': 2})
# print(my_dict)
#
# my_dict |= {200: 1, '300': 2}
# print(my_dict)
#
# my_dict = {
#     100: {
#         'a': 'b',
#         'b': 'c',
#     },
#     200: {
#         1: 2,
#         3: [1, 2, 3]
#     },
# }
#
# print(my_dict[100]['b'])
# print(my_dict[200][3])
# my_dict[200][3].append('4')
#
# print(my_dict)
#
# lst = [1, '1234', []]
#
# print(type(lst[0]))
# print(type(lst[1]))
# print(type(lst[2]))

"""
1 Напишіть код, який зформує строку, яка містить певну інформацію про символ за його номером у слові.
Наприклад "The [номер символу] symbol in '[тут слово]' is '[символ з відповідним порядковим номером в слові]'".
Слово та номер символу отримайте за допомогою input() або скористайтеся константою.
Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in 'Python' is 't' ".


2 Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
Напишіть код, який сформує новий list (наприклад lst2), який би містив всі числові змінні (int, float), які є в lst1.
Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.

3 Дана довільна строка. Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві голосні літери підряд.

4 Є два довільних числа які відповідають за мінімальну і максимальну ціну.
Є Dict з назвами магазинів і цінами:
{
"cito": 47.999,
"BB_studio" 42.999,
"momo": 49.999,
"main-service": 37.245,
"buy.now": 38.324,
"x-store": 37.166,
"the_partner": 38.988,
"store": 37.720,
"rozetka": 38.003
}.
Напишіть код, який знайде і виведе на екран назви магазинів,
ціни яких попадають в діапазон між мінімальною і максимальною ціною. Наприклад:

lower_limit = 35.9
upper_limit = 37.339

> match: "x-store", "main-service"


"""
