# functional vs OOP

# main principles

# class

# a = '10'
# print(a.isdigit())
#
#
# a = int(10)
# print(a)


# class MyClass:
#
#     a = 10
#     b = 20
#
#
# my_obj = MyClass()
#
# # print(type(my_obj))
#
# my_obj2 = MyClass()
#
# print(my_obj.a)
# print(my_obj2.a)
# print(my_obj.b)
# print(my_obj2.b)

#
# class MyClass:
#
#     a = 10
#     b = 20
#
#
# my_obj = MyClass()
#
# print(my_obj.a)
# my_obj.a = 30
# print(my_obj.a)
#
# my_obj.c = 40
# print(my_obj.c)
#
# print(dir(my_obj))


# class MyClass:
#
#     a = 10
#     b = 20
#
#     def foo(self):  # self = my_obj
#         print(f'I\'m a function {self.a}, {self.b}')
#
#
# my_obj = MyClass()
#
# my_obj.foo()  # MyClass.foo(my_obj)


# class MyClass:
#
#     a = 10
#     b = 20
#
#     def foo(self):  # self = my_obj
#         print(f'I\'m a function {self.a}, {self.b}')
#
#
# my_obj = MyClass()
# my_obj2 = MyClass()
#
# my_obj.a = 50
# my_obj.foo()
#
#
# my_obj2.a = 100
# my_obj2.foo()


# class MyClass:
#
#     a = 10
#     b = 20
#
#     def foo(self, arg):  # self = my_obj
#         print(f'I\'m a function {self.a}, {arg}')
#
#
# my_obj = MyClass()
# my_obj2 = MyClass()
#
# my_obj.a = 50
# my_obj.foo('Hello')
#
#
# my_obj2.a = 100
# my_obj2.foo('World')


# class User:
#
#     name = 'Artem'
#     age = 40
#
#     def say_hello(self):
#         print(f'Hello, I\'m {self.name}, I\'m {self.age} years old')
#
#
# u1 = User()
#
# u2 = User()
# u2.age = 20
#
# u1.say_hello()
# u2.say_hello()


# class User:
#     name = None
#     age = None
#     city = None
#
#     def __init__(self, new_age, new_name):
#         self.age = new_age
#         self.name = new_name
#
#     def say_hello(self):
#         print(f'Hello, I\'m {self.name}, I\'m {self.age} years old')
#
#
# u1 = User(40, 'Artem_programmer')
# u2 = User(20, 'Artem_student')
#
#
# # print(dir(u1))
# # u1.say_hello()
# # u2.say_hello()


# class User:
#     name = None
#     age = None
#     city = None
#
#     def __init__(self, new_age, new_name):
#         self.age = new_age
#         self.name = new_name
#
#     def say_hello(self):
#         print(f'Hello, I\'m {self.name}, I\'m {self.age} years old')
#
#
# class Programmer(User):
#     language = None
#
#     def __init__(self, new_age, new_name, lang):
#         self.age = new_age
#         self.name = new_name
#         self.language = lang
#
#     def say_hello(self):
#         print(f'Hello, I\'m {self.name}, I\'m {self.age} years old, I use {self.language}')
#
#
# u1 = User(40, 'Artem')
#
# p1 = Programmer(40, 'Artem', 'Python')
#
# print(dir(u1))
# print(dir(p1))


# u1.say_hello()
# p1.say_hello()




