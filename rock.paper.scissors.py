import random

items = ["R", "P", "S"]


def player_input_item():
    while True:
        player_choice = input(f"Rock, Paper, Scissors, SHOOT choice your item from the list {items}:  ").upper()
        if player_choice in items:
            break
    return str(player_choice)


def dealer_random_item():
    dealer_choice = random.choice(items)
    return str(dealer_choice)


def game_play(dealer, player):
    game = [dealer, player]
    index_of_winner = 0
    if "R" in game and "P" in game:
        return game.index("P")
    elif "R" in game and "S" in game:
        return game.index("R")
    elif "P" in game and "S" in game:
        return game.index("S")
    else:
        return 2


def winner_announcement(winner_index):
    if winner_index == 1:
        print('Player wins!!!')
    elif winner_index == 0:
        print("dealer wins :(")
    elif winner_index == 2:
        print("Tie")


def game_launcher():
    player_choice = player_input_item()
    delar_choice = dealer_random_item()
    winner = game_play(delar_choice, player_choice)
    winner_announcement(winner)
    print(f"dealer showed {delar_choice}. You showed {player_choice}")


game_launcher()
