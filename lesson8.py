# functions
#
# def foo(arg1, arg2=0):
#     print(arg1)
#     print(arg2)
#     return 0
#
#
# res = foo(1)
# res = foo(1, 2)


# def foo(*args):
#     print(type(args))
#     print(args)
#     for i in args:
#         print(f'arg -> {i}')
#
#
# foo(1, '2', True, [], 5.5, {})

#
# def foo(arg1, arg2=0,  *args):
#     print(type(args))
#     print(args)
#     for i in args:
#         print(f'arg -> {i}')
#
#
# foo(1, '2', True, [], 5.5, {})


# def foo(a, b, **kwargs):
#     print(type(kwargs))
#     print(kwargs)
#     for i in kwargs:
#         print(f'kwarg -> {i}')
#
#
# foo(a=10, b=20, c=30)


# def foo(a, b, c=0, d=1, *args, **kwargs):
#     print(args)
#     print(kwargs)
#
#
# # foo(1, 2, 3, 4, 5, 6, 7, 8, x=2, y=3, z=0)
#
# args_list = [1, 2, 3, 4, 5, 6]
#
# foo(*args_list)  # foo(1, 2, 3, 4, 5, 6)
#
#
# args_dct = {'x': 1, 'y': 3, 'z': 4}
#
# # foo(**args_dct)  # foo(a=1, b=3, c=4)
#
# foo(1, 2, c=10, *args_list, **args_dct)


def function_name(list_to_add):

    list_to_add.append(1)
    return list_to_add


lst = [1, 2, 3]

print(lst)

lst = function_name(lst)

print(lst)


"""
1. Напшіть функцію month_to_season(), яка отримує один аргумент - номер місяця - та повертає назву сезону, 
до якого відноситься цей місяць.
Наприклад, передаємо 2 - отримуємо "Зима"

2. Напишіть функцію яка отримує отримує один аргумент - текст довільної довжин і повертає два слова - 
найбільш часто вживане в даному тексті і найдовше


3. "Тупий калькулятор". Напишіть функцію dumb_calc(a, b, operation) яка отримує два числових аргументи і один 
стрінговий (який описує операцію між числовими аргументами) і повертає результат виконання операції між аргументами.
Припустимі операції - "+" (додати), "*" (помножити), "-" (відняти), "/" (розділити) 
В усіх інших випадках функція повинна повернути "Операція не підтримується"
"""


