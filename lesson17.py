# class User:
#     name = 'Bill'
#     age = None
#     city = None
#
#     def __init__(self, new_age, new_name):
#         self.age = new_age
#         self.name = new_name
#
#     def __sub__(self, other):
#         return self
#
#     def __len__(self) -> int:
#         return self.age
#
#     def __contains__(self, item) -> bool:
#         return item in self.name
#
#
# u = User(20, 'Jane')
#
# print(len(u))
# print('d' in u)


# # iterator
#
# class User:
#     name = 'Bill'
#     age = None
#     city = None
#
#     def __init__(self, new_age, new_name):
#         self.age = new_age
#         self.name = new_name
#
#
# u = User(20, 'Jane')
#
# for i in 'qwertyu':
#     print(i)
#
#
# # s = 'qwertyu'
# # it = iter(s)
# # while True:
# #     try:
# #         print(next(it))
# #     except StopIteration:
# #         break
# #
#
#
# for i in 'qwertyu':  # it = iter('qwertyu')
#     # try:
#     #     i = next(it)
#     # except StopIteration:
#     #     break
#     print(i)


# class MyIterator:
#     limit = 0
#     current = 0
#
#     def __init__(self, limit):
#         self.limit = limit
#
#     def __iter__(self):
#         self.current = 0
#         return self
#
#     def __next__(self):
#         print('next ->')
#         if self.current > self.limit:
#             raise StopIteration
#         self.current += 1
#         return self.current - 1
#
#
# it = MyIterator(5)
#
#
# for i in it:
#     print('inside loop', i)
#
# print('-------------------')
#
# for i in it:
#     print('inside loop', i)
#

# def generator_function():
#     print('before 1')
#     yield 1
#     print('after 1')
#
#     print('before 2')
#     yield 2
#     print('after 2')
#
#     print('before 3')
#     yield 3
#     print('after 3')
#
#
# generator = generator_function()
#
# print(generator)
#
# for i in generator:
#     print(i)


def generator_counter(limit):
    print('-> generator_counter')
    current = 0
    while True:
        print('next', current)
        yield current
        current += 1
        if current > limit:
            return


generator = generator_counter(5)

print(generator)

for i in generator:
    print('--->', i)


class User:
    name = 'Bill'
    age = None
    city = None

    def __init__(self, new_age, new_name):
        self.age = new_age
        self.name = new_name


u = User(20, 'Jane')

if u:  # bool(u)
    pass
